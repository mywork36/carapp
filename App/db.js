const mysql = require('mysql')

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '<qweasz>',
  port: 3306,
  database: 'sunbeam',
  connectionLimit: 10,
})

module.exports = {
  pool,
}
