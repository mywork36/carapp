const express = require('express')

const app = express()

const carRouter = require('./car')
app.use(express.json())
app.use('/car', carRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('Server started on port 4000')
})
