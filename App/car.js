const { request, response } = require('express')
const express = require('express')
const db = require('./db')
const util = require('./utils')

const router = express.Router()

router.get('/get/:id', (request, response) => {
  const { id } = request.params
  const statment = `select car_name,company_name,car_prize from car where car_id=?`
  db.pool.query(statment, [id], (error, car) => {
    response.send(util.createResult(error, car))
  })
})
router.post('/post', (request, response) => {
  const { car_name, company_name, car_prize } = request.body
  const statment = `Insert into car (car_name,company_name,car_prize) values (?,?,?)`
  db.pool.query(statment, [car_name, company_name, car_prize], (error, car) => {
    response.send(util.createResult(error, car))
  })
})
router.put('/update/:id', (request, response) => {
  const { id, car_name, company_name, car_prize } = request.body
  const statment = `update car set car_name= ?, company_name=?,car_prize=? where car_id=?`
  db.pool.query(
    statment,
    [id, car_name, company_name, car_prize],
    (error, car) => {
      response.send(util.createResult(error, car))
    }
  )
})
router.delete('/delete/:id', (request, response) => {
  const { id } = request.params
  const statment = `delete from car where car_id=?`
  db.pool.query(statment, [id], (error, car) => {
    response.send(util.createResult(error, car))
  })
})
module.exports = router
